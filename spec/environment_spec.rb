require 'machine'
require 'machine_group'
require 'environment'
require 'yaml'

RSpec.describe Environment do
    before(:example) do
        @machine_group1 = MachineGroup.new
        @machine_group1.set_name("group1")
        @machine1 = Machine.new
        @machine1.set_name("machine1")
        @machine1.set_public_ipv4("10.200.74.101", "16")
        @machine1.set_private_ipv4("192.168.1.1", "24")
        @machine1.set_public_ipv6("fd00:0:0:a::101", "64")
        @machine_group1.add_machine(@machine1)
        @machine_group1.add_variable('var_c', 'value_c')

        @machine_group2 = MachineGroup.new
        @machine_group2.set_name("group2")
        @machine2 = Machine.new
        @machine2.set_name("machine2")
        @machine2.set_public_ipv4("10.200.74.102", "16")
        @machine2.set_private_ipv4("192.168.1.2", "24")
        @machine2.set_public_ipv6("fd00:0:0:a::102", "64")
        @machine_group2.add_machine(@machine2)
        @machine_group2.add_variable('var_d', 'value_d')

        @machine_group3 = MachineGroup.new
        @machine_group3.set_name("group3")
        @machine3 = Machine.new
        @machine3.set_name("machine3")
        @machine3.set_public_ipv4("10.200.74.102", "16")
        @machine3.set_private_ipv4("192.168.1.2", "24")
        @machine3.set_public_ipv6("fd00:0:0:a::102", "64")
        @machine_group3.add_machine(@machine3)

        @machine_group4 = MachineGroup.new
        @machine_group4.set_name("group4")
        @machine4 = Machine.new
        @machine4.set_name("machine4")
        @machine4.set_public_ipv4("10.200.74.102", "16")
        @machine4.set_private_ipv4("192.168.1.2", "24")
        @machine4.set_public_ipv6("fd00:0:0:a::102", "64")
        @machine_group4.add_machine(@machine4)

        file_hash = YAML.load_file('files/servers.yml')
        @environment_hash = nil
        file_hash.each_pair do |environment_name, data|
            if environment_name == 'environment1'
                @environment_hash = { environment_name => data }
            end
        end
    end

    context "with an Environment class object", "#initialize" do
        it "creates an Environment class object" do
            environment = Environment.new

            expect(environment).to_not be_nil
        end
    end

    context "with an Environment class object", "#set_name" do
        it "creates an Environment class object" do
            environment = Environment.new
            environment.set_name("my_environment")

            expect(environment).to respond_to(:set_name)
            expect(environment.name).to eq "my_environment"
        end
    end

    context "with an Environment class object", "#add_machine_group" do
        it "adds a machine group" do
            environment = Environment.new
            environment.set_name("my_environment")
            environment.add_machine_group(@machine_group1)

            entry = @machine_group1.inventory_entry
            expect(environment.inventory_entry).to eq entry
        end
    end

    context "with an Environment class object", "#inventory_entry" do
        it "adds a machine group" do
            environment = Environment.new
            environment.add_machine_group(@machine_group1)
            environment.add_machine_group(@machine_group2)

            entry = @machine_group1.inventory_entry
            entry += "\n\n"
            entry += @machine_group2.inventory_entry

            expect(environment.inventory_entry).to eq entry
        end
    end

    context "with an Environment class object", "#add_group_var" do
        it "responds to the method add_group_var" do
            environment = Environment.new

            expect(environment).to respond_to(:add_group_var)
        end
    end

    context "with an Environment class object", "#get_group_var" do
        it "responds to the method get_group_var" do
            environment = Environment.new

            expect(environment).to respond_to(:get_group_var)
        end
    end

    context "with an Environment class object", "#get_group_var" do
        it "add a variable for an inventory group and recall it" do
            environment = Environment.new
            environment.add_group_var("var_a", "value_a")

            expect(environment.get_group_var("var_a")).to eq "value_a"
        end
    end

    context "with an Environment class object", "#group_vars_entry" do
        it "returns an ansible group entry string" do
            environment = Environment.new
            environment.add_group_var("var_a", "value_a")
            environment.add_group_var("var_b", "value_b")
            vars = Hash.new
            vars['var_a'] = 'value_a'
            vars['var_b'] = 'value_b'
            entry = vars.to_yaml

            expect(environment.group_vars_entry()).to eq entry
        end
    end

    context "with an Environment class object", "#set_(some)_include_vars" do
        it "adds a variable to a include_var file named some" do
            environment = Environment.new

            environment.set_random_include_vars("var_a", "value_a")
        end
    end

    context "with an Environment class object", "#get_(some)_include_vars" do
        it "get value of a variable from an include_var file named some" do
            environment = Environment.new
            environment.set_random_include_vars("var_a", "value_a")

            expect(environment.get_random_include_vars("var_a")).to eq "value_a"
        end
    end

    context "with an Environment class object", "#(some)_include_var_entry" do
        it "returns an include_var entry string from a file named some" do
            environment = Environment.new
            environment.set_random_include_vars("var_a", "value_a")
            environment.set_random_include_vars("var_b", "value_b")
            vars = Hash.new
            vars['var_a'] = 'value_a'
            vars['var_b'] = 'value_b'
            entry = vars.to_yaml

            expect(environment.random_include_vars_entry).to eq entry
        end
    end

    context "with an Environment class object", "#to_s" do
        it "returns a string representation of the environment" do
            environment1 = Environment.new
            environment1.set_name("environment1")

            machine_group1 = MachineGroup.new
            machine_group1.set_name("group_name1")
            machine1 = Machine.new
            machine1.set_name("machine1")
            machine1.set_public_ipv4("10.200.74.101", "16")
            machine2 = Machine.new
            machine2.set_name("machine2")
            machine2.set_public_ipv4("10.200.74.102", "16")
            machine_group1.add_machine(machine1)
            machine_group1.add_machine(machine2)
            machine_group1.add_variable('var_a', 'value_a')

            machine_group2 = MachineGroup.new
            machine_group2.set_name("group_name2")
            machine3 = Machine.new
            machine3.set_name("machine3")
            machine3.set_public_ipv4("10.200.74.103", "16")
            machine4 = Machine.new
            machine4.set_name("machine4")
            machine4.set_public_ipv4("10.200.74.104", "16")
            machine_group2.add_machine(machine3)
            machine_group2.add_machine(machine4)

            environment1.add_machine_group(machine_group1)
            environment1.add_machine_group(machine_group2)

            environment1.set_random_include_vars("var_a", "value_a")
            environment1.set_other_include_vars("var_b", "value_b")

            out = "environment1\n\n"
            out += machine_group1.to_s.gsub(/(.*)/, '    \1')
            out += "\n"
            out += machine_group2.to_s.gsub(/(.*)/, '    \1')
            out += "\n"
            out += environment1.group_vars_entry().to_s.gsub(/(.*)/, '    \1')

            expect(environment1.to_s).to eq out
        end
    end

    context "with an Environment class object", "#==" do
        it "correctly sees equality when variable hash order differs" do
            environment1 = Environment.new
            environment1.set_name("environment1")
            environment1.add_group_var('var_a', 'value_a')
            environment1.add_group_var('var_b', 'value_b')

            environment2 = Environment.new
            environment2.set_name("environment1")
            environment2.add_group_var('var_b', 'value_b')
            environment2.add_group_var('var_a', 'value_a')

            expect(environment1==(environment2)).to be true
        end
    end

    context "with an Environment class object", "#write" do
        it "responds to the write method" do
            environment = Environment.new

            expect(environment).to respond_to(:write)
        end
    end

    context "with an Environment class object", "#write" do
        it "writes out a valid Ansible inventory" do
            environment = Environment.new
            environment.set_name("test_environment")
            environment.add_machine_group(@machine_group1)
            environment.add_machine_group(@machine_group2)
            environment.add_group_var("var_a", "value_a")

            path = File.expand_path("..", File.dirname(__FILE__))
            env_path = Pathname.new(path) + "environments"
            test_path = env_path + environment.name

            environment.write(env_path)

            expect(Dir.exists?(test_path)).to be true
            expect(File.exists?(test_path + "inventory")).to be true
            expect(File.exists?(test_path + "group_vars" + "all.yml")).to be true
            expect(File.exists?(test_path + "group_vars" + "group1.yml")).to be true
            expect(File.exists?(test_path + "group_vars" + "group2.yml")).to be true

            if (Dir.exists?(env_path))
                FileUtils.remove_dir(env_path)
            end
        end
    end

    context "with an Environment class object", "#configure_with" do
        it "responds to configure_with method" do
            environment = Environment.new

            expect(environment).to respond_to(:configure_with)
        end
    end

    context "with an Environment class object", "#configure_with" do
        it "takes its configuration entirely from a data structure" do
            environment1 = Environment.new
            environment1.configure_with(@environment_hash)

            environment2 = Environment.new
            environment2.set_name("environment1")
            environment2.add_machine_group(@machine_group1)
            environment2.add_machine_group(@machine_group2)
            environment2.add_group_var('var_a', 'value_a')
            environment2.add_group_var('var_b', 'value_b')

            expect(environment1==(environment2)).to be true
        end
    end
end
