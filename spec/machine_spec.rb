require 'machine'
require 'ipaddr'

RSpec.describe Machine do

    context "with a Machine class object", "#initialize" do
        it "can instantiate the object" do
            machine = Machine.new

            expect(machine).to_not be_nil
        end
    end

    context "with a Machine class object", "#set_name" do
        it "responds to the set_name method" do
            machine = Machine.new

            expect(machine).to respond_to(:set_name)
        end
    end

    context "with a Machine class object", "#set_name" do
        it "sets the machine name" do
            name = "test_machine_name"

            machine = Machine.new
            machine.set_name(name)

            expect(machine.name).to eq name
        end
    end

    context "with a Machine class object", "#set_public_ipv4" do
        it "sets the public ipv4 address" do
            public_ipv4 = "10.200.74.101"
            public_ipv4_prefix = 16
            public_ipv4_network = "10.200.0.0"

            machine = Machine.new
            machine.set_public_ipv4(public_ipv4, public_ipv4_prefix)

            expect(machine.public_ipv4).to eq public_ipv4
            expect(machine.public_ipv4_prefix).to eq public_ipv4_prefix
            expect(machine.public_ipv4_network).to eq public_ipv4_network
            expect(machine.public_ipv4_netmask).to eq '255.255.0.0'
            expect { machine.set_public_ipv4("cat", "cat") }.to raise_error(IPAddr::InvalidAddressError)
            expect { machine.set_public_ipv4(public_ipv4, "cat") }.to raise_error(IPAddr::InvalidAddressError)
        end
    end

    context "with a Machine class object", "#set_private_ipv4" do
        it "sets the private ipv4 address" do
            private_ipv4 = "192.168.1.1"
            private_ipv4_prefix = 24
            private_ipv4_network = "192.168.1.0"

            machine = Machine.new
            machine.set_private_ipv4(private_ipv4, private_ipv4_prefix)

            expect(machine.private_ipv4).to eq private_ipv4
            expect(machine.private_ipv4_prefix).to eq private_ipv4_prefix
            expect(machine.private_ipv4_network).to eq private_ipv4_network
            expect(machine.private_ipv4_netmask).to eq '255.255.255.0'

            expect { machine.set_private_ipv4("cat", "cat") }.to raise_error(IPAddr::InvalidAddressError)
            expect { machine.set_private_ipv4(private_ipv4, "cat") }.to raise_error(IPAddr::InvalidAddressError)
        end
    end

    context "with a Machine class object", "#set_public_ipv6" do
        it "sets the public ipv6 address" do
            public_ipv6 = "fd00:0:0:a::101"
            public_ipv6_prefix = 64
            public_ipv6_network = "fd00:0:0:a::"

            machine = Machine.new
            machine.set_public_ipv6(public_ipv6, public_ipv6_prefix)

            expect(machine.public_ipv6).to eq public_ipv6
            expect(machine.public_ipv6_prefix).to eq public_ipv6_prefix
            expect(machine.public_ipv6_network).to eq public_ipv6_network
            expect(machine.public_ipv6_netmask).to eq 'ffff:ffff:ffff:ffff:0000:0000:0000:0000'
            expect { machine.set_public_ipv6("cat", "cat") }.to raise_error(IPAddr::InvalidAddressError)
            expect { machine.set_public_ipv6(public_ipv6, "cat") }.to raise_error(IPAddr::InvalidAddressError)
        end
    end

    context "with a Machine class object", "#inventory_entry" do
        it "returns an ansible inventory entry string" do
            machine_name = "test_machine_name"
            public_ipv4 = "10.200.74.101"
            public_ipv4_prefix = 16
            public_ipv4_network = "10.200.0.0"

            machine = Machine.new
            machine.set_name(machine_name)
            machine.set_public_ipv4(public_ipv4, public_ipv4_prefix)

            expect(machine.inventory_entry).to eq "#{public_ipv4} machine_name=#{machine_name}"
        end
    end

    context "with a Machine class object", "#==" do
        it "returns true if two objects internals are equal" do
            machine_name = "my_name"
            public_ipv4 = "10.200.74.101"
            public_ipv4_prefix = 16
            public_ipv4_network = "10.200.0.0"
            private_ipv4 = "192.168.1.1"
            private_ipv4_prefix = 24
            private_ipv4_network = "192.168.1.0"
            public_ipv6 = "fd00:0:0:a::101"
            public_ipv6_prefix = 64
            public_ipv6_network = "fd00:0:0:a::"

            machine1 = Machine.new
            machine1.set_name(machine_name)
            machine1.set_public_ipv4(public_ipv4, public_ipv4_prefix)
            machine1.set_private_ipv4(private_ipv4, private_ipv4_prefix)
            machine1.set_public_ipv6(public_ipv6, public_ipv6_prefix)

            machine2 = Machine.new
            machine2.set_name(machine_name)
            machine2.set_public_ipv4(public_ipv4, public_ipv4_prefix)
            machine2.set_private_ipv4(private_ipv4, private_ipv4_prefix)
            machine2.set_public_ipv6(public_ipv6, public_ipv6_prefix)

            expect(machine1==(machine2)).to be true
        end
    end

    context "with a Machine class object", "#==" do
        it "returns false if two objects have different machine names" do
            machine1 = Machine.new
            machine1.set_name("machine1")

            machine2 = Machine.new
            machine2.set_name("machine2")

            expect(machine1==(machine2)).to be false
        end
    end

    context "with a Machine class object", "#==" do
        it "returns false if two objects have a different public ipv4 address" do
            common_prefix = 16

            machine1 = Machine.new
            machine1.set_public_ipv4("192.168.1.100", common_prefix)

            machine2 = Machine.new
            machine2.set_public_ipv4("192.168.1.101", common_prefix)

            expect(machine1==(machine2)).to be false
        end
    end

    context "with a Machine class object", "#==" do
        it "returns false if two objects have different public ipv4 prefixes" do
            common_ip = "192.168.1.100"

            machine1 = Machine.new
            machine1.set_public_ipv4(common_ip, "16")

            machine2 = Machine.new
            machine2.set_public_ipv4(common_ip, "24")

            expect(machine1==(machine2)).to be false
        end
    end

    context "with a Machine class object", "#==" do
        it "returns false if two objects have a different private ipv4 address" do
            common_prefix = 16

            machine1 = Machine.new
            machine1.set_private_ipv4("192.168.1.100", common_prefix)

            machine2 = Machine.new
            machine2.set_private_ipv4("192.168.1.101", common_prefix)

            expect(machine1==(machine2)).to be false
        end
    end

    context "with a Machine class object", "#==" do
        it "returns false if two objects have different private ipv4 prefixes" do
            common_ip = "192.168.1.100"

            machine1 = Machine.new
            machine1.set_private_ipv4(common_ip, "16")

            machine2 = Machine.new
            machine2.set_private_ipv4(common_ip, "24")

            expect(machine1==(machine2)).to be false
        end
    end

    context "with a Machine class object", "#==" do
        it "returns false if two objects have a different public ipv6 address" do
            common_prefix = 64

            machine1 = Machine.new
            machine1.set_public_ipv6("fd00:0:0:a::101", common_prefix)

            machine2 = Machine.new
            machine2.set_public_ipv6("fd00:0:0:b::101", common_prefix)

            expect(machine1==(machine2)).to be false
        end
    end

    context "with a Machine class object", "#==" do
        it "returns false if two objects have different public ipv6 prefixes" do
            common_ip = "fd00:0:0:a::101"

            machine1 = Machine.new
            machine1.set_public_ipv6(common_ip, 64)

            machine2 = Machine.new
            machine2.set_public_ipv6(common_ip, 80)

            expect(machine1==(machine2)).to be false
        end
    end

    context "with a Machine class object", "#to_s" do
        it "returns a basic string output of the object contents" do
            machine_name = "my_name"
            public_ipv4 = "10.200.74.101"
            public_ipv4_prefix = 16
            public_ipv4_network = "10.200.0.0"
            private_ipv4 = "192.168.1.1"
            private_ipv4_prefix = 24
            private_ipv4_network = "192.168.1.0"
            public_ipv6 = "fd00:0:0:a::101"
            public_ipv6_prefix = 64
            public_ipv6_network = "fd00:0:0:a::"

            machine = Machine.new
            machine.set_name(machine_name)
            machine.set_public_ipv4(public_ipv4, public_ipv4_prefix)
            machine.set_private_ipv4(private_ipv4, private_ipv4_prefix)
            machine.set_public_ipv6(public_ipv6, public_ipv6_prefix)

            out = "#{machine_name}\n"
            out += "    #{public_ipv4}/#{public_ipv4_prefix} on #{public_ipv4_network}\n"
            out += "    #{private_ipv4}/#{private_ipv4_prefix} on #{private_ipv4_network}\n"
            out += "    #{public_ipv6}/#{public_ipv6_prefix} on #{public_ipv6_network}"

            expect(machine.to_s).to eq out
        end
    end

    context "with a Machine class object", "#configure_with" do
        it "has the configure_with method" do
            machine = Machine.new

            expect(machine).to respond_to(:configure_with)
        end
    end

    context "with a Machine class object", "#configure_with" do
        it "can load from a hash of configuration data" do
            machine_name = "my_name"
            public_ipv4 = "10.200.74.101"
            public_ipv4_prefix = 16
            private_ipv4 = "192.168.1.1"
            private_ipv4_prefix = 24
            public_ipv6 = "fd00:0:0:a::101"
            public_ipv6_prefix = 64
            machine_hash = { machine_name =>
                             {"public_ipv4"=>public_ipv4,
                              "public_ipv4_prefix"=>public_ipv4_prefix,
                              "private_ipv4"=>private_ipv4,
                              "private_ipv4_prefix"=>private_ipv4_prefix,
                              "public_ipv6"=>public_ipv6,
                              "public_ipv6_prefix"=>public_ipv6_prefix }
            }

            machine1 = Machine.new
            machine1.set_name(machine_name)
            machine1.set_public_ipv4(public_ipv4, public_ipv4_prefix)
            machine1.set_private_ipv4(private_ipv4, private_ipv4_prefix)
            machine1.set_public_ipv6(public_ipv6, public_ipv6_prefix)

            machine2 = Machine.new
            machine2.configure_with(machine_hash)

            expect(machine1==(machine2)).to be true
        end
    end
end
