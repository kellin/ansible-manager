require 'machine'
require 'machine_group'
require 'yaml'

RSpec.describe MachineGroup do
    before(:example) do

        public_ipv4 = "10.200.74.101"
        public_ipv4_prefix = 16
        private_ipv4 = "192.168.1.1"
        private_ipv4_prefix = 24
        public_ipv6 = "fd00:0:0:a::101"
        public_ipv6_prefix = 64

        @machine1_hash = { "machine1" =>
                           {"public_ipv4"=>public_ipv4,
                            "public_ipv4_prefix"=>public_ipv4_prefix,
                            "private_ipv4"=>private_ipv4,
                            "private_ipv4_prefix"=>private_ipv4_prefix,
                            "public_ipv6"=>public_ipv6,
                            "public_ipv6_prefix"=>public_ipv6_prefix }
        }
        @machine1 = Machine.new
        @machine1.configure_with(@machine1_hash)

        @machine2_hash = { "machine2" =>
                           {"public_ipv4"=>public_ipv4,
                            "public_ipv4_prefix"=>public_ipv4_prefix,
                            "private_ipv4"=>private_ipv4,
                            "private_ipv4_prefix"=>private_ipv4_prefix,
                            "public_ipv6"=>public_ipv6,
                            "public_ipv6_prefix"=>public_ipv6_prefix }
        }
        @machine2 = Machine.new
        @machine2.configure_with(@machine2_hash)
    end

    context "with a new MachineGroup class object", "#initialize"  do
        it "can instantiate the object" do
            machine_group = MachineGroup.new

            expect(machine_group).to_not be_nil
        end
    end

    context "with a MachineGroup class object", "#set_name" do
        it "responds to the set_name method" do
            machine_group = MachineGroup.new

            expect(machine_group).to respond_to(:set_name)
        end
    end

    context "with a MachineGroup class object", "#set_name" do
        it "responds to the set_name method" do
            name = "test_machine_group_name"

            machine_group = MachineGroup.new
            machine_group.set_name(name)

            expect(machine_group.name).to eq name
        end
    end

    context "with a new MachineGroup class object", "#add_machine" do
        it "adds a Machine to the Machine Group" do
            machine_group = MachineGroup.new
            machine_group.add_machine(@machine1)
        end
    end

    context "with a new MachineGroup class object", "#size" do
        it "gets the number of machines in the Machine Group" do
            machine_group = MachineGroup.new
            machine_group.add_machine(@machine1)

            expect(machine_group.size).to eq 1

            # don't expect duplicates to increment count
            machine_group.add_machine(@machine1)
            expect(machine_group.size).to eq 1

            # add a new machine count should increment
            machine_group.add_machine(@machine2)
            expect(machine_group.size).to eq 2
        end
    end

    context "with a new MachineGroup class object", "#inventory_entry" do
        it "returns an ansible inventory entry string" do
            machine_group = MachineGroup.new
            machine_group.set_name("my_group")
            machine_group.add_machine(@machine1)
            machine_group.add_machine(@machine2)

            entry = "[#{machine_group.name}]\n"
            entry += "#{@machine1.inventory_entry}\n"
            entry += "#{@machine2.inventory_entry}"
            expect(machine_group.inventory_entry).to eq entry
        end
    end

    context "with a new MachineGroup class object", "#add_variable" do
        it "responds to #add_variable call" do
            machine_group = MachineGroup.new

            expect(machine_group).to respond_to(:add_variable)
        end
    end

    context "with a new MachineGroup class object", "#get_variable" do
        it "responds to #get_variable call" do
            machine_group = MachineGroup.new

            expect(machine_group).to respond_to(:get_variable)
        end
    end

    context "with a new MachineGroup class object", "#get_variable" do
        it "adds a Variable to the Machine Group" do
            machine_group = MachineGroup.new
            machine_group.add_variable("var_a", "value_a")

            expect(machine_group.get_variable("var_a")).to eq "value_a"
        end
    end

    context "with a new MachineGroup class object", "#var_entry" do
        it "returns an ansible group entry string" do
            machine_group = MachineGroup.new
            machine_group.set_name("my_group")
            machine_group.add_variable("var_a", "value_a")
            vars = Hash.new
            vars['var_a'] = 'value_a'
            entry = vars.to_yaml

            expect(machine_group.vars_entry).to eq entry
        end
    end

    context "with a new MachineGroup class object", "#to_s" do
        it "returns a basic string output of the object contents" do
            machine_group = MachineGroup.new
            machine_group.set_name("my_group")
            machine_group.add_machine(@machine1)
            machine_group.add_variable("var_a", "value_a")

            out = "my_group\n\n"
            out += @machine1.to_s.gsub(/(.*)/, '    \1')
            out += "\n\n"
            out += machine_group.vars_entry.gsub(/(.*)/, '    \1')

            expect(machine_group.to_s).to eq out
        end
    end

    context "with a MachineGroup class object", "#==" do
        it "returns true if two objects have equal contents" do
            machine_group_a = MachineGroup.new
            machine_group_a.set_name("group_same")

            machine_group_b = MachineGroup.new
            machine_group_b.set_name("group_same")

            machine_group_a.add_machine(@machine1)
            machine_group_b.add_machine(@machine1)

            expect(machine_group_a.==(machine_group_b)).to be true
        end
    end

    context "with a MachineGroup class object", "#configure_with" do
        it "has the configure_with method" do
            expect(MachineGroup.new).to respond_to(:configure_with)
        end
    end

    context "with a MachineGroup class object", "#configure_with" do
        it "can load from a hash of configuration data" do
            machine_group_name = "my_group_name"
            machine_group_hash = { machine_group_name =>
                                   @machine1_hash.merge(@machine2_hash)}

            machine_group1 = MachineGroup.new
            machine_group1.set_name(machine_group_name)
            machine_group1.add_machine(@machine1)
            machine_group1.add_machine(@machine2)

            machine_group2 = MachineGroup.new
            machine_group2.configure_with(machine_group_hash)

            expect(machine_group1==(machine_group2)).to be true
        end
    end

    context "with a MachineGroup class object", "#variable_entry" do
        it "can return a MachineGroup variable list YAML formatted" do
            machine_group = MachineGroup.new
            machine_group.add_variable("var_a", "value_a")
            machine_group.add_variable("var_b", "value_b")

            output = { "var_a" => "value_a",
                       "var_b" => "value_b" }.to_yaml

            expect(machine_group).to respond_to(:variable_entry)
            expect(machine_group.variable_entry).to eq output
        end
    end
end
