require 'ansible_configuration'
require 'pathname'
require 'machine'
require 'machine_group'

RSpec.describe AnsibleConfiguration do
    before(:example) do
        @manager = AnsibleConfiguration.new
        @config_fn = Pathname(__FILE__).dirname.parent + "files/servers.yml"

        # mock up the environments as they exist in the test file
        @environment_name1 = "environment1"
        @environment1 = Environment.new
        @environment1.set_name(@environment_name1)

        @machine_group1 = MachineGroup.new
        @machine_group1.set_name("group1")
        @machine1 = Machine.new
        @machine1.set_name("machine1")
        @machine1.set_public_ipv4("10.200.74.101", "16")
        @machine1.set_private_ipv4("192.168.1.1", "24")
        @machine1.set_public_ipv6("fd00:0:0:a::101", "64")
        @machine_group1.add_machine(@machine1)

        @machine_group2 = MachineGroup.new
        @machine_group2.set_name("group2")
        @machine2 = Machine.new
        @machine2.set_name("machine2")
        @machine2.set_public_ipv4("10.200.74.102", "16")
        @machine2.set_private_ipv4("192.168.1.2", "24")
        @machine2.set_public_ipv6("fd00:0:0:a::102", "64")
        @machine_group2.add_machine(@machine2)

        @environment_name2 = "environment1"
        @environment2 = Environment.new
        @environment2.set_name(@environment_name2)

        @machine_group3 = MachineGroup.new
        @machine_group3.set_name("group3")
        @machine3 = Machine.new
        @machine3.set_name("machine3")
        @machine3.set_public_ipv4("10.200.74.103", "16")
        @machine3.set_private_ipv4("192.168.1.3", "24")
        @machine3.set_public_ipv6("fd00:0:0:a::103", "64")
        @machine_group3.add_machine(@machine3)

        @machine_group4 = MachineGroup.new
        @machine_group4.set_name("group4")
        @machine4 = Machine.new
        @machine4.set_name("machine4")
        @machine4.set_public_ipv4("10.200.74.104", "16")
        @machine4.set_private_ipv4("192.168.1.4", "24")
        @machine4.set_public_ipv6("fd00:0:0:a::104", "64")
        @machine_group4.add_machine(@machine4)
    end
    context "with an Ansible Manager class object", "#initialize" do
        it "load the module" do
            expect(@manager).to_not be_nil
        end
    end
    context "with an Ansible Manager class object" do
        it "loads environments from a file", "#load_from_file" do
            expect(File.exists?(@config_fn)).to be true
        end
    end
end
