require 'machine'

class MachineGroup
    attr_reader :name

    def initialize()
        @name = nil
        @machines = Hash.new
        @variables = Hash.new
    end

    def configure_with(hash_data)
        hash_data.each_pair do |group_name, machine_data|
            self.set_name(group_name)

            machine_data.each_pair do |machine_name, config|
                if machine_name == 'vars'
                    config.each_pair do |name, value|
                        self.add_variable(name, value)
                    end
                else
                    machine = Machine.new
                    machine.configure_with({machine_name => config})
                    self.add_machine(machine)
                end
            end
        end
    end

    def set_name(name)
        @name = name
    end

    def add_machine(machine)
        @machines[machine.name] = machine
    end

    def size
        @machines.size
    end

    def inventory_entry
        entry = "[#{name}]"
        @machines.each_pair do |name, machine|
            entry += "\n#{machine.inventory_entry}"
        end
        return entry
    end

    def variable_entry
        return self.vars_entry
    end

    def add_variable(name, value)
        @variables[name] = value
    end

    def get_variable(name)
        return @variables[name]
    end

    def vars_entry
        if @variables.empty?
            return nil
        end

        return @variables.to_yaml
    end

    def to_s
        out = @name
        @machines.each_pair do |name, machine|
            out += "\n\n"
            out += machine.to_s.gsub(/(.*)/, '    \1')
        end
        out += "\n\n"
        out += vars_entry.to_s.gsub(/(.*)/, '    \1')

        return out
    end

    def ==(group)
        if to_s == group.to_s
            return true
        else
            return false
        end
    end
end
