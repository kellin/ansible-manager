class Environment
    attr_reader :name

    def initialize()
        @name = nil
        @machine_groups = Hash.new
        @group_vars = Hash.new
        @include_vars_files = Hash.new
    end

    def set_name(name)
        @name = name
    end

    def configure_with(hash_data)
        hash_data.each_pair do |env_name, group_data|
            self.set_name(env_name)

            group_data.each_pair do |group_name, config|
                if group_name == 'vars'
                    config.each_pair do |name, value|
                        self.add_group_var(name, value)
                    end
                else
                    machine_group = MachineGroup.new
                    machine_group.configure_with({group_name => config})
                    self.add_machine_group(machine_group)
                end
            end
        end
    end

    def add_machine_group(machine_group)
        @machine_groups[machine_group.name] = machine_group
    end

    def inventory_entry
        entry = ""
        @machine_groups.each_pair do |group_name, group|
            if entry.length > 0
                entry += "\n\n"
            end
            entry += "#{group.inventory_entry}"
        end
        return entry
    end

    def write(env_path)
        if (@name.nil?)
            return
        end

        path = Pathname.new(env_path) + @name
        FileUtils.mkdir_p(path)

        inventory_file = File.open(path + "inventory", 'w')
        inventory_file.write(self.inventory_entry)
        inventory_file.close

        group_vars_path = path + "group_vars"

        if (@group_vars.size > 0)
            FileUtils.mkdir_p(group_vars_path)

            group_file = File.open(group_vars_path + "all.yml", 'w')
            group_file.write(self.group_vars_entry())
            group_file.close
        end

        @machine_groups.each_pair do |group_name, group|
            if group.variable_entry.nil?
                next
            end
            FileUtils.mkdir_p(group_vars_path)
            group_file = File.open(group_vars_path + "#{group_name}.yml", 'w')
            group_file.write(group.vars_entry)
            group_file.close
        end
    end

    def add_group_var(var_name, var_value)
        @group_vars[var_name] = var_value
    end

    def get_group_var(var_name)
        return @group_vars[var_name]
    end

    def group_vars_entry()
        if @group_vars.nil?
            return nil
        end

        keys = @group_vars.keys.sort
        array = keys.map{ |key| [key, self.get_group_var(key)] }

        return Hash[array].to_yaml
    end

    def method_missing(m, *args, &block)
        method_name = m.to_s
        set_include_expression = Regexp.new('^set_(.*)_include_vars$')
        get_include_expression = Regexp.new('^get_(.*)_include_vars$')
        include_entry_expression = Regexp.new('^(.*)_include_vars_entry$')

        if not set_include_expression.match(method_name).nil?
            file_name = method_name[set_include_expression,1]
            set_include_vars_variable(file_name, *args)
        elsif not get_include_expression.match(method_name).nil?
            file_name = method_name[get_include_expression,1]
            get_include_vars_variable(file_name, *args)
        elsif not include_entry_expression.match(method_name).nil?
            file_name = method_name[include_entry_expression, 1]
            include_vars_entry(file_name)
        else
            super
        end
    end

    def set_include_vars_variable(file_name, var_name, var_value)
        if @include_vars_files[file_name].nil?
            @include_vars_files[file_name] = Hash.new
        end

        @include_vars_files[file_name][var_name] = var_value
    end

    def get_include_vars_variable(file_name, var_name)
        if @include_vars_files[file_name].nil?
            return nil
        elsif @include_vars_files[file_name][var_name].nil?
            return nil
        else
            return @include_vars_files[file_name][var_name]
        end
        return nil
    end

    def include_vars_entry(file_name)
        unless @include_vars_files[file_name].nil?
            return @include_vars_files[file_name].to_yaml
        end
        return Hash.new
    end

    def ==(other)
        if to_s == other.to_s
            return true
        else
            return false
        end
    end

    def to_s
        out = @name + "\n"
        @machine_groups.each_pair do |group_name, group|
            out += "\n" + group.to_s.gsub(/(.*)/, '    \1')
        end
        out += "\n" + self.group_vars_entry().to_s.gsub(/(.*)/, '    \1')
        return out
    end
end
