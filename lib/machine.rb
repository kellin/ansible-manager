require 'ipaddr'

class IPAddr
  def netmask
      _to_string(@mask_addr)
  end
end

class Machine
    attr_reader :name
    attr_reader :public_ipv4, :private_ipv4, :public_ipv6
    attr_reader :public_ipv4_network, :private_ipv4_network
    attr_reader :public_ipv6_network
    attr_reader :public_ipv4_prefix, :private_ipv4_prefix
    attr_reader :public_ipv6_prefix
    attr_reader :public_ipv4_netmask, :private_ipv4_netmask
    attr_reader :public_ipv6_netmask

    def initialize()
        @name = nil
    end

    def set_name(name)
        @name = name
    end

    def configure_with(hash_data)
        # TODO: need to validate hash_data
        hash_data.each do | name, config |
            self.set_name(name)
            if(config.nil?)
                next
            end
            if config.has_key?("public_ipv4") and config.has_key?("public_ipv4_prefix")
                self.set_public_ipv4(config["public_ipv4"],
                                     config["public_ipv4_prefix"])
            end
            if config.has_key?("private_ipv4") and config.has_key?("private_ipv4_prefix")
                self.set_private_ipv4(config["private_ipv4"],
                                     config["private_ipv4_prefix"])
            end
            if config.has_key?("public_ipv6") and config.has_key?("public_ipv6_prefix")
                self.set_public_ipv6(config["public_ipv6"],
                                     config["public_ipv6_prefix"])
            end
        end
    end

    def set_public_ipv4(address, prefix)
        @public_ipv4 = IPAddr.new(address)
        @public_ipv4_prefix = prefix
        @public_ipv4_network = @public_ipv4.mask(@public_ipv4_prefix)
        @public_ipv4_netmask = @public_ipv4_network.netmask
    end

    def set_private_ipv4(address, prefix)
        @private_ipv4 = IPAddr.new(address)
        @private_ipv4_prefix = prefix
        @private_ipv4_network = @private_ipv4.mask(@private_ipv4_prefix)
        @private_ipv4_netmask = @private_ipv4_network.netmask
    end

    def set_public_ipv6(address, prefix)
        @public_ipv6 = IPAddr.new(address)
        @public_ipv6_prefix = prefix
        @public_ipv6_network = @public_ipv6.mask(@public_ipv6_prefix)
        @public_ipv6_netmask = @public_ipv6_network.netmask
    end

    def inventory_entry
        return "#{@public_ipv4} machine_name=#{@name}"
    end

    def ==(other)
        if to_s == other.to_s
            return true
        else
            return false
        end
    end

    def to_s
        out = "#{@name}\n"
        out += "    #{@public_ipv4}/#{@public_ipv4_prefix} on #{@public_ipv4_network}\n"
        out += "    #{@private_ipv4}/#{@private_ipv4_prefix} on #{@private_ipv4_network}\n"
        out += "    #{@public_ipv6}/#{@public_ipv6_prefix} on #{@public_ipv6_network}"
    end
end
